@echo off

:: �� ���������� ���������� ���䨣��樨 1� 7.7 � �ᯮ�짮������ gcomp
:: �ᯮ������� 4 ��⠫���:
:: %org_old �ਣ����쭠� ���䨣����, ���� ५��
:: %org_new �ਣ����쭠� ���䨣����, ���� ५��
:: %wrk_old ���� ࠡ��� ���䨣���� (���������� org_old)
:: %wrk_new ����� ࠡ��� ���䨣���� (������塞�� org_new)
::
:: � ������ �� ��� ���� �����⠫�� SRC\ � ��室������ 䠩�� 1cv7.md
:: ������, ��室��� � Far Manager � �����⠫���, ���ਬ��,
:: %wrk_old%\SRC\���㬥���\���室�멎थ� ����� ������� � ��⠫��
:: %org_old%\SRC\���㬥���\���室�멎थ�.
::
::
:: ���� SetEnv - ���३ ��쪮
:: ���ᠭ��  - http://plugring.farmanager.com/plugin.php?pid=762
:: ��室���� - https://github.com/trexinc/evil-programmers/tree/master/SetEnv
::

 rem ����㧪� ����஥�
 call %~dp0.\goto1s.conf.cmd
 
 goto %1 


:org_old
 set gp=%org_old%
 goto proc

:org_new
 set gp=%org_new%
 goto proc

:wrk_old
 set gp=%wrk_old%
 goto proc

:wrk_new
 set gp=%wrk_new%
 goto proc


:proc

set str1=%~dp2%
set str2=%str1:\SRC\=*%

for /F "delims=* tokens=1,2" %%a in ("%str2%") do (
  set p1=%%a
  set p2=%%b
)

setenv path1s=%gp%\SRC\%p2%  /IM Far.exe
