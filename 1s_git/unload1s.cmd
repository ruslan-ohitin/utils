@echo off
:: Unload 1S database from current dir to text archive in upper dir

:: Ruslan Ohitin 
:: ruslan.ohitin@gmail.com 
:: +7 903 999 6557


 set bin1s=C:\base\1Cv77\BIN\1cv7.exe

 rem get current date as YYYYMMDD
 set curdate=%date:~-4%%date:~3,2%%date:~0,2%

 set db=%CD%
:: Check that this dir contains 1S config?

 set tfile="%TEMP%\unload1s-%RANDOM%.txt"

:: 1S need all paths in win1251 codepage
 chcp 1251 > nul

 echo [General] > %tfile%
 echo Output=%db%-%curdate%.txt >> %tfile%
 echo Quit=1 >> %tfile%
 echo CheckAndRepair=0 >> %tfile%
 echo UnloadData=1 >> %tfile%
 echo SaveData=0 >> %tfile%
 echo [UnloadData] >> %tfile%
 echo UnloadToFile=%db%-%curdate%.zip  >> %tfile%
 echo IncludeUserDef=1 >> %tfile%

 "%bin1s%" config /M /D "%db%\" /@%tfile%

 del %tfile%